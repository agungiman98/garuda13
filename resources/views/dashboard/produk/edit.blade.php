@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Edit Produk</h1>
</div>

<div class="col-lg-8">
<form method="post" action="/dashboard/produk/{{ $produk->id }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required autofocus value="{{ old('name', $produk->name ) }}">
      @error('name')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>

    <div class="form-group">
      <label for="stock">stock</label>
      <input type="number" class="form-control @error('stock') is-invalid @enderror" id="stock" name="stock" required value="{{ old('stock',  $produk->stock ) }}">
      @error('stock')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>

    <div class="form-group">
      <label for="harga">harga</label>
      <input type="number" class="form-control @error('harga') is-invalid @enderror" id="harga" name="harga" required value="{{ old('harga',  $produk->harga ) }}">
      @error('harga')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>

    <div class="form-group">
      <label for="category" class="form-label">Category</label>
      <select class="custom-select" id="category" name="category_id">
        @foreach ($categories as $category)
        @if(old('category_id', $produk->category_id) == $category->id)
        <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
        @else 
        <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endif
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="user" class="form-label">Nama Update Stock</label>
      <select class="custom-select" id="user" name="user_id">
        @foreach ($users as $user)
        @if(old('user_id') == $user->id)
        <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
        @else 
        <option value="{{ $user->id }}">{{ $user->name }}</option>
        @endif
        @endforeach
      </select>
    </div>

    <div class="custom-file mb-3">
     
      <input type="hidden" name="oldImage" value="{{ $produk->image }}">
      @if($produk->image)
      <img class="img-preview img-fluid my-3 col-sm-8" src="{{ asset('storage/' . $produk->image) }}">
      @else
      <img class="img-preview img-fluid my-3 col-sm-8">
      @endif

      <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="image" name="image" onchange="previewImage()">
      <label class="custom-file-label" for="image">Produk Image</label>
      @error('image')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
  
    </div>  
    
    <div class="form-group">
      <label for="body" class="form-label @error('body') is-invalid @enderror">Body</label>
      <input id="body" type="hidden" name="body" value="{{ old('body', $produk->body) }}">
      <trix-editor input="body"></trix-editor>
      @error('body')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>




      <button type="submit" class="btn btn-primary my-3">Update Post</button>

  </form>
</div>


  <script>


    document.addEventListener('trix-file-accept', function(e){
      e.preventDefault();
    });

    function previewImage() {
      
      const image = document.querySelector('#image');
      const imgPrev = document.querySelector('.img-preview'); 
      
      imgPrev.style.display = 'block';

      const oFReader = new FileReader();
      oFReader.readAsDataURL(image.files[0]);

      oFReader.onload = function(oFREvent){
        imgPrev.src = oFREvent.target.result;
      }
    }

  </script>



@endsection