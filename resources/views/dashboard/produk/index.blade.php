@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Produk</h1>
</div>

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show col-lg-8" role="alert">
  {{ session('success') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(session()->has('delete'))
<div class="alert alert-success alert-dismissible fade show col-lg-8" role="alert">
  {{ session('delete') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="table-responsive col-lg-8">
  <a href="/dashboard/produk/create" class="btn btn-success btn-sm my-2">Tambah Produk Baru</a>
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Category</th>
          <th>Stock</th>
          <th>Harga</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($produk as $p)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $p->name }}</td>
          <td>{{ $p->category->name }}</td>
          <td>{{ $p->stock }}</td>
          <td>{{ $p->harga }}</td>
          <td>
            <a href="/dashboard/produk/{{ $p->id }}/edit" class="badge bg-warning text-light">
                <span data-feather="edit"></span>
            </a>

            <form action="/dashboard/produk/{{ $p->id }}" method="post" class="d-inline">
              @csrf
              @method('DELETE')
                <button type="submit" class="badge bg-danger text-light border-0" onclick="return confirm('Yakin ?')">
                      <span data-feather="trash-2"></span>
                </button>
          </form>

          </td>
        </tr> 
        @endforeach
      </tbody>
    </table>
  </div>




    
@endsection