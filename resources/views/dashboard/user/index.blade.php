@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">User Controller</h1>
</div>

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show col-lg-8" role="alert">
  {{ session('success') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(session()->has('delete'))
<div class="alert alert-success alert-dismissible fade show col-lg-8" role="alert">
  {{ session('delete') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="table-responsive col-lg-8">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Email</th>
          <th>Admin</th>
          <th>Control</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($users as $user)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $user->name }}</td>
          <td>{{ $user->email }}</td>
          <td>
            @if ($user->is_admin === 1)
                yes
            @else
                No
            @endif
          </td>
          <td>
            <a href="/dashboard/user/{{ $user->id }}/edit" class="badge bg-warning text-light">
                <span data-feather="edit"></span>
            </a>

            <form action="/dashboard/user/{{ $user->id }}" method="post" class="d-inline">
              @csrf
              @method('DELETE')
                <button type="submit" class="badge bg-danger text-light border-0" onclick="return confirm('Yakin ?')">
                      <span data-feather="trash-2"></span>
                </button>
          </form>

          </td>
        </tr> 
        @endforeach
      </tbody>
    </table>
  </div>




    
@endsection