@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Edit User</h1>
</div>

<div class="col-lg-8">
<form method="post" action="/dashboard/user/{{ $user->id }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required autofocus value="{{ old('name', $user->name ) }}">
      @error('name')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>

    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" required autofocus value="{{ old('email', $user->email ) }}">
      @error('email')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    

      
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline1" name="is_admin" class="custom-control-input" value="1" autofocus>
        <label class="custom-control-label" for="customRadioInline1">Master Admin</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline2" name="is_admin" class="custom-control-input" value="0">
        <label class="custom-control-label" for="customRadioInline2">Basic Admin</label>
      @error('is_admin')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror

      <button type="submit" class="btn btn-primary mt-5 my-3 ">Update Post</button>

  </form>
</div>




@endsection