@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Edit Category</h1>
</div>

<div class="col-lg-8">
<form method="post" action="/dashboard/categories/{{ $category->id }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required autofocus value="{{ old('name', $category->name ) }}">
      @error('name')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>


    <div class="custom-file mb-3">
     
      <input type="hidden" name="oldImage" value="{{ $category->image }}">
      @if($category->image)
      <img class="img-preview img-fluid my-3 col-sm-8" src="{{ asset('storage/' . $category->image) }}">
      @else
      <img class="img-preview img-fluid my-3 col-sm-8">
      @endif

      <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="image" name="image" onchange="previewImage()">
      <label class="custom-file-label" for="image">Category Image</label>
      @error('image')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
  
    </div>  
    



      <button type="submit" class="btn btn-primary my-3">Update</button>

  </form>
</div>


  <script>



    function previewImage() {
      
      const image = document.querySelector('#image');
      const imgPrev = document.querySelector('.img-preview'); 
      
      imgPrev.style.display = 'block';

      const oFReader = new FileReader();
      oFReader.readAsDataURL(image.files[0]);

      oFReader.onload = function(oFREvent){
        imgPrev.src = oFREvent.target.result;
      }
    }

  </script>



@endsection