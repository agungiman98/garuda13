@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Create New Category</h1>
</div>

<div class="col-lg-8">
<form method="post" action="/dashboard/categories" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required autofocus value="{{ old('name') }}">
      @error('name')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>



    <div class="custom-file mb-3">
     
      <img class="img-preview img-fluid my-3 col-sm-8">
      <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="image" name="image" onchange="previewImage()">
      <label class="custom-file-label" for="image">Category Image</label>
      @error('image')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
  
    </div>    




      <button type="submit" class="btn btn-primary my-3">Create Category</button>

  </form>
</div>


  <script>

    function previewImage() {
      
      const image = document.querySelector('#image');
      const imgPrev = document.querySelector('.img-preview'); 
      
      imgPrev.style.display = 'block';

      const oFReader = new FileReader();
      oFReader.readAsDataURL(image.files[0]);

      oFReader.onload = function(oFREvent){
        imgPrev.src = oFREvent.target.result;
      }
    }

    document.addEventListener('trix-file-accept', function(e){
      e.preventDefault();
    });

    // /* Dengan Rupiah */
    // var dengan_rupiah = document.getElementById('harga');
    // dengan_rupiah.addEventListener('keyup', function(e)
    // {
    //     dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
    // });
    
    // /* Fungsi */
    // function formatRupiah(angka, prefix)
    // {
    //     var number_string = angka.replace(/[^,\d]/g, '').toString(),
    //         split    = number_string.split(','),
    //         sisa     = split[0].length % 3,
    //         rupiah     = split[0].substr(0, sisa),
    //         ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
    //     if (ribuan) {
    //         separator = sisa ? '.' : '';
    //         rupiah += separator + ribuan.join('.');
    //     }
        
    //     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    //     return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    // }

  </script>



@endsection