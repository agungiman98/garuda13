@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Categories Controller</h1>
</div>

    @if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show col-lg-6" role="alert">
  {{ session('success') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(session()->has('delete'))
<div class="alert alert-success alert-dismissible fade show col-lg-6" role="alert">
  {{ session('delete') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="table-responsive col-lg-6">
  <a href="/dashboard/categories/create" class="btn btn-success btn-sm my-2">Create New Category</a>
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>#</th>
          <th>Category Name</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($categories as $category)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $category->name }}</td>
          <td>
            <a href="/dashboard/categories/{{ $category->id }}/edit" class="badge bg-warning text-light">
                <span data-feather="edit"></span>
            </a>

            <form action="/dashboard/categories/{{ $category->id }}" method="post" class="d-inline">
              @csrf
              @method('DELETE')
                <button type="submit" class="badge bg-danger text-light border-0" onclick="return confirm('Yakin ?')">
                      <span data-feather="trash-2"></span>
                </button>
          </form> 

          </td>
        </tr> 
        @endforeach
      </tbody>
    </table>
  </div>

@endsection