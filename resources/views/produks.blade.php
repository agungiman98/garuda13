@extends('layouts.main')
@section('content')
    
{{-- header --}}
<div class="container mt-3">
    <div class="row text-center">
        <div class="col-lg-12">
            <h1>
                Detail Semua Produk
            </h1>
            <hr>
        </div>
    </div>
</div>
{{-- header End --}}

{{-- category --}}
<div class="container mt-3">
    <div class="row text-center">
        <div class="col-lg-12">
            <h1>
                Category
            </h1>

        </div>
    </div>
</div>
<div class="container mt-3">
    <div class="row text-center">
        <div class="btn-group btn-group-toggle col-md-12" data-toggle="buttons">
          @foreach($categories as $category)
            <label class="btn btn-secondary ">
              <a href="#" class="text-decoration-none  text-light"> {{ $category->name }}</a>
            </label>
            @endforeach
        </div>
    </div>
</div>
<hr>
  {{-- category end --}}

{{-- Produk All --}}
<div class="container-fluid ">
    <div class="row d-flex justify-content-around">
   
     
   @foreach($produks as $produk)
   <div class="card text-center m-3 col-md-2" style="width: 18rem;">
       <img src="{{ asset('storage/' . $produk->image) }}" class="card-img-top" alt="...">
       <div class="card-body ">
         <h5 class="card-title"><a href="" class="text-decoration-none text-dark">{{ $produk->name }}</a></h5>
         <p class="card-text">{!! $produk->body !!}</p>
         <p class="card-text">Rp. {{ $produk->harga }}</p>
         <p class="card-text">Stock: {{ $produk->stock }}</p>

         <div class="container">
            <div class="row">
              <form action="{{ route('cart.store') }}" class="form-inline" method="post">
                @csrf
                <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                <label for="qty"></label>
                <input type="number" class="form-control col-md-4 m-1" name="qty" id="qty">
                <button type="submit" class="btn btn-primary btn-sm col-md-4 m-1">Add To Cart</button>
              </form>
            </div>
          </div>
       
       </div>
     </div>
   @endforeach
  
   
   
   </div>
   </div>

{{-- produk end --}}





@endsection