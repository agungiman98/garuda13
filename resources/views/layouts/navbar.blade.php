    {{-- navbar --}}

    <nav class="navbar navbar-expand-lg navbar-light bg-warning py-1">
        <a class="navbar-brand" href="/">Garuda 13</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse mx-3" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/produk">Produk</a>
            </li>
          </ul>


          {{-- sign in navbar --}}
          <ul class="navbar-nav ml-auto">
            @auth
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                Halo, {{ auth()->user()->name }}
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('dashboard') }}"><i class="bi bi-layout-wtf"></i> DashBoard</a>
                <div class="dropdown-divider"></div>
      
      
                <form action="/logout" method="post">
                  @csrf
                  @method('POST')
                <button type="submit" class="dropdown-item">
                  <i class="bi bi-box-arrow-right"></i> Log Out
                </button>
                </form>
            @else

            @livewire('cart-counter')
                   

            <li class="nav-item m-2 text-center">
                <a class="nav-link" href="/login"><i class="bi bi-box-arrow-in-right"></i> Login</a>
            </li>
            @endauth
          </ul>
        </div>
      </nav>

      {{-- endNav --}}