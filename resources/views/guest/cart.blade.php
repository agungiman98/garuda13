@extends('layouts.main')
@section('content')

@if(session()->has('success'))
<div class="container">
  <div class="row">
    <div class="alert alert-success alert-dismissible fade show col-lg-8" role="alert">
      {{ session('success') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @elseif(session()->has('error'))
    <div class="alert alert-success alert-dismissible fade show col-lg-8" role="alert">
      {{ session('error') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">Jumlah Barang</th>
                    <th scope="col">Harga Satuan</th>
                    <th scope="col">Total Harga</th>
                    <th scope="col">Option</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($cartItems as $item)    
                  <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->qty }}</td>
                    <td>{{ $item->price }}</td>
                    <td>{{ $item->price * $item->qty }}</td>
                    <td>
                        hapus
                    </td>
                  </tr>
                  @endforeach
                  <tr>
                    <th scope="row">#</th>
                    <td colspan="3"><h5>Total Harga</h5></td>
                    <td colspan="2" class="bg-success"></td>
                  </tr>
                </tbody>
              </table>
        </div>
    </div>
</div>

  

    
@endsection