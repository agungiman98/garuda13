@extends('layouts.main')
@section('content')



<div class="row justify-content-center my-4">
    <div class="col-md-6">
        <body class="text-center">
            <h1 class="h3 mb-3 font-weight-normal text-center my-4">Registration Form</h1>
            <form class="form-registration" method="POST">
                @csrf
                @method('POST')
                    <input type="text" id="name" name="name" class="form-control rounded-top @error('name') is-invalid @enderror required"  placeholder="Name" value="{{ old('name') }}">
                    <label for="name" class="sr-only">Name</label>
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>   
                    @enderror

                    <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror required" placeholder="Email address" value="{{ old('email') }}">
                    <label for="email" class="sr-only">Email address</label>
                    @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>   
                    @enderror
                
                    <input type="password" name="password" id="password" class="form-control rounded-bottom @error('password') is-invalid @enderror required" placeholder="Password" required>
                    <label for="password" class="sr-only">Password</label>
                    @error('password')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>   
                    @enderror
                
                <button class="btn btn-lg btn-primary btn-block mt-4" type="submit">Register</button>
            </form>
            <small><a href="/login">Back to Login</a></small>
        </body>
    </div>
</div>








    
@endsection