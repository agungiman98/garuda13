@extends('layouts.main')
@section('content')


<div class="row justify-content-center my-4">
    <div class="col-md-5">

        @if (session()->has('success'))         
        <div class="alert alert-success alert-dismissible fade show text-left" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif

        @if (session()->has('error'))         
        <div class="alert alert-danger alert-dismissible fade show text-left" role="alert">
            {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif


        <body class="text-center">
            <h1 class="h3 mb-3 font-weight-normal text-center my-4">Please sign in</h1>
            <form class="form-signin" method="post" action="{{ route('authenticate') }}">
                @csrf
                @method("POST")
                <label for="email" class="sr-only"></label>
                <input type="email" id="email" class="form-control @error('email') is-invalid @enderror mb-3" placeholder="Email address" name="email" required autofocus >
                @error('email')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>   
                @enderror

                <label for="password" class="sr-only"></label>
                <input type="password" id="password" class="form-control @error('password') is-invalid @enderror mb-3" placeholder="Password" name="password" required ">
                @error('password')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror

                <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">Sign in</button>
                
            </form>
            <small class="mt-4"><a href="/register">Register</a></small>
        </body>
    </div>
</div>








@endsection