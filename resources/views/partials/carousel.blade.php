{{-- carousel start --}}
<div class="container">
<div class="row justify-content-center my-3">
    <div class="col-lg-12">
    
        <div class="owl-carousel owl-theme owl-loaded">
            <div class="owl-stage-outer" style="width: 100%">
                <div class="owl-stage">
                    <div class="owl-item align-content-center" style="width: 800px; height:600px"><img src="{{ asset('img/1.jpg') }}" style="max-height: 600px; display: block; margin: auto; width: 100%;" alt=""></div>
                    <div class="owl-item align-content-center" style="width: 800px; height:600px"><img src="{{ asset('img/2.jpg') }}" style="max-height: 600px; display: block; margin: auto; width: 100%;" alt=""></div>
                    <div class="owl-item align-content-center" style="width: 800px; height:600px"><img src="{{ asset('img/3.jpg') }}" style="max-height: 600px; display: block; margin: auto; width: 100%;" alt=""></div>
                </div>
            </div>
        </div>
    
    </div>
</div>
</div>
    {{-- carousel end --}}