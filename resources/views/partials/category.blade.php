{{-- Category --}}
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center mt-5 mb-3">
            <h1>
                Category
            </h1>
        </div>
    </div>
</div>

<div class="container ">
    <div class="row d-flex justify-content-around">

        
            @foreach ($categories as $category)         
            <div class="card col-md-4 text-center" style="width: 18rem;">
                <img src="{{ asset('storage/' . $category->image  )}}" class="card-img-top" alt="...">
                <div class="card-body">
                <h5 class="card-title">{{ $category->name }}</h5>
                <a href="#" class="btn btn-primary">Pilih Category</a>
                </div>
            </div>
            @endforeach

            
        


</div>
</div>
{{-- category end --}}