<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $guarded = ['id'];
    use HasFactory;


    public function cart()
    {
        return $this->hasMany(Cart::class);
    }
}
