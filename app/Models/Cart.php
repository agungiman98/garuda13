<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Gloudemans\Shoppingcart\CanBeBought;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cart extends Model implements Buyable
{
    protected $guarded = ['id'];
    use CanBeBought;
    use HasFactory;

    public function produk()
    {
        return $this->hasMany(Produk::class);
    }
    public function pembeli()
    {
        return $this->belongsTo(Pembeli::class);
    }

    protected $tables = 'shoppingcart';
}
