<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProdukTable extends Component
{
    public function render()
    {
        return view('livewire.produk-table');
    }
}
