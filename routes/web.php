<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardProdukController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
use App\Models\Category;
use App\Models\Produk;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// halaman Index Utama
Route::get('/', function () {
    return view('index', [
        'categories' => Category::all(),
        'produks' => Produk::all()
    ]);
});

// halaman menampilkan Semua Produk
Route::get('/produk', function(){
    return view('produks', [
        'categories' => Category::all(),
        'produks' => Produk::all()
    ]);
});

// Login and Logout Route
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate'])->name('authenticate');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
// End Login and Logout Route


// Register Route
Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'store']);
// End Of Register Route


Route::get('/dashboard', function(){
    return view('dashboard.index');
})->middleware('auth')->name('dashboard');


// Produk pada Dashboard Route
Route::resource('/dashboard/produk', DashboardProdukController::class)->middleware('auth')->except('show');

// cart
Route::resource('/cart', CartController::class)->middleware('guest');

// admin Category
Route::resource('/dashboard/categories', CategoryController::class)->except('show')->middleware('master');

// user Controller
Route::resource('/dashboard/user', UserController::class)->except('show', 'create')->middleware('master');
