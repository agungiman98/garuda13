<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::create([
            'name' => 'admin',
            'is_admin' => 1,
            'email' => 'admin@gmail.com',
            'password' => bcrypt('12345')
        ]);

        Category::create([
            'name' => 'Makanan',
            'image' => '10'
            
        ]);
        Category::create([
            'name' => 'Minuman',
            'image' => '11'
            
        ]);
        Category::create([
            'name' => 'Snack',
            'image' => '12'
            
        ]);
    }
}
